import { WidgetApi, IWidget } from "../interfaces";
import { useState, useEffect, useRef, useCallback } from "react";

function useWidget(api: WidgetApi) {
  const [widgets, setWidgets] = useState<IWidget[]>([]);
  const isMountedRef = useRef(false);

  const addOne = useCallback(
    (widget: IWidget) => {
      setWidgets((state) => {
        return [...state, widget];
      });
    },
    [setWidgets]
  );

  const deleteOne = useCallback(
    (id: string) => {
      setWidgets((state) => {
        return state.filter((i) => String(i.id) !== id);
      });
    },
    [setWidgets]
  );

  useEffect(() => {
    setWidgets(api.read());
    setTimeout(() => {
      isMountedRef.current = true; // set next tick to avoid write
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isMountedRef.current) {
      api.write(widgets);
    }
  }, [widgets, api]);

  return {
    actions: {
      addOne,
      deleteOne,
    },
    widgets,
  };
}

export default useWidget;
