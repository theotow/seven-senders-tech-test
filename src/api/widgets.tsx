import { IWidget } from "../interfaces";

const STORAGE_KEY = "widgets";

const read = (): IWidget[] => {
  const state = window.localStorage.getItem(STORAGE_KEY) || "[]";
  return JSON.parse(state);
};

const write = (state: IWidget[]) => {
  window.localStorage.setItem(STORAGE_KEY, JSON.stringify(state));
};

export { read, write };
