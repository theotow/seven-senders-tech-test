import React from "react";
import useWidget from "../hooks/useWidget";
import * as widgetApi from "../api/widgets";

export const StateContext = React.createContext<ReturnType<
  typeof useWidget
> | null>(null);

const StateContextProvider: React.FC<{}> = (props) => {
  const widget = useWidget(widgetApi);
  return (
    <StateContext.Provider value={widget}>
      {props.children}
    </StateContext.Provider>
  );
};

export default StateContextProvider;
