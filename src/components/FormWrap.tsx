import React from "react";
import css from "./FormWrap.module.scss";

const FormWrap: React.FC = (props) => {
  return <div className={css.form__wrap}>{props.children}</div>;
};

export default FormWrap;
