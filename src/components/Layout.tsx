import React from "react";
import { Layout } from "antd";
import css from "./Layout.module.scss";

const WidgetList: React.FC = (props) => {
  return (
    <Layout className={css.layout}>
      <Layout.Content className={css.layout__content}>
        {props.children}
      </Layout.Content>
    </Layout>
  );
};

export default WidgetList;
