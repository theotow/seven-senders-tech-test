import React from "react";
import { Route, Switch } from "react-router-dom";
import "antd/dist/antd.css";
import WidgetAdd from "./WidgetAdd";
import WidgetDelete from "./WidgetDelete";
import WidgetList from "./WidgetList";

function App() {
  return (
    <React.Fragment>
      <Switch>
        <Route exact path="/">
          <WidgetList />
        </Route>
        <Route exact path="/add">
          <WidgetAdd />
        </Route>
      </Switch>
      <Route path="/delete/:id">
        <WidgetDelete />
      </Route>
    </React.Fragment>
  );
}

export default App;
