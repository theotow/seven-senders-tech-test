import React, { useContext } from "react";
import Modal from "antd/lib/modal/Modal";
import { useHistory, useParams } from "react-router-dom";
import { StateContext } from "./StateContext";
import { IStateContext } from "../interfaces";

function WidgetDelete() {
  const widget = useContext(StateContext) as IStateContext;
  const history = useHistory();
  const { id } = useParams<{ id: string }>();

  return (
    <Modal
      title="Delete Widget"
      centered
      visible
      onOk={() => {
        widget.actions.deleteOne(id);
        history.push("/");
      }}
      onCancel={() => history.push("/")}
    >
      <p>Are you sure you want to delete widget({id})?</p>
    </Modal>
  );
}

export default WidgetDelete;
