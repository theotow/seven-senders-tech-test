import React, { useState, useContext } from "react";
import { Form, Input, Button, Steps } from "antd";
import { useHistory } from "react-router-dom";
import { INextable, IStateContext, IWidget } from "../interfaces";
import { StateContext } from "./StateContext";
import Layout from "./Layout";
import FormWrap from "./FormWrap";

function StepOne(props: INextable) {
  return (
    <Form
      name="basic"
      initialValues={props.widgetState}
      onFinish={(s) => props.next({ ...props.widgetState, ...s })}
    >
      <Form.Item
        label="Name"
        name="name"
        rules={[{ required: true, message: "Please input name!" }]}
      >
        <Input />
      </Form.Item>
      <Button type="primary" htmlType="submit">
        {props.btnText}
      </Button>
    </Form>
  );
}

function StepTwo(props: INextable) {
  return (
    <Form
      name="basic"
      initialValues={props.widgetState}
      onFinish={(s) => props.next({ ...props.widgetState, ...s })}
    >
      <Form.Item
        label="Language"
        name="language"
        rules={[{ required: true, message: "Please input language!" }]}
      >
        <Input />
      </Form.Item>
      <Button type="primary" htmlType="submit">
        {props.btnText}
      </Button>
    </Form>
  );
}

function WidgetAdd() {
  const [widgetToAdd, updateWidget] = useState<IWidget>({
    name: "",
    language: "",
    id: "",
  });
  const [currentStep, setStep] = useState(0);
  const widget = useContext(StateContext) as IStateContext;
  const history = useHistory();

  return (
    <Layout>
      <Steps current={currentStep}>
        <Steps.Step title="Name" />
        <Steps.Step title="Language" />
      </Steps>
      <FormWrap>
        {currentStep === 0 && (
          <StepOne
            widgetState={widgetToAdd}
            btnText="Next"
            next={(widgetState) => {
              updateWidget(widgetState);
              setStep((step) => step + 1);
            }}
          />
        )}
        {currentStep === 1 && (
          <StepTwo
            widgetState={widgetToAdd}
            btnText="Create"
            next={(widgetState) => {
              widget.actions.addOne({
                name: widgetState.name,
                language: widgetState.language,
                id: Math.random().toString(),
              });
              history.push("/");
            }}
          />
        )}
      </FormWrap>
    </Layout>
  );
}

export default WidgetAdd;
