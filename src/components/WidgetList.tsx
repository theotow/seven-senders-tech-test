import React, { useContext } from "react";
import { Table, Button, Space } from "antd";
import { Link } from "react-router-dom";
import { StateContext } from "./StateContext";
import { IStateContext } from "../interfaces";
import Layout from "./Layout";

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Language",
    dataIndex: "language",
    key: "language",
  },
  {
    title: "Action",
    dataIndex: "id",
    key: "id",
    render: (id: string) => <Link to={`/delete/${id}`}>Delete</Link>,
  },
];

function WidgetList() {
  const widget = useContext(StateContext) as IStateContext;

  return (
    <Layout>
      <Space direction="vertical" style={{ width: "100%" }}>
        <Link to="/add">
          <Button type="primary">Add</Button>
        </Link>
        <Table
          dataSource={widget.widgets.map((i) => ({ ...i, key: i.id }))}
          columns={columns}
          pagination={false}
        />
      </Space>
    </Layout>
  );
}

export default WidgetList;
