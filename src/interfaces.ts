import useWidget from "./hooks/useWidget";

export interface IWidget {
  id: string;
  name: string;
  language: string;
}

export interface WidgetApi {
  read(): IWidget[];
  write(state: IWidget[]): void;
}

export interface INextable {
  widgetState: IWidget;
  btnText: string;
  next(widget: IWidget): void;
}

export type IStateContext = ReturnType<typeof useWidget>;
